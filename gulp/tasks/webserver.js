/* Requires */

const gulp = require('gulp')
const gls = require('gulp-live-server')
const browserSync = require('browser-sync').create()

/* Tasks */

gulp.task('start-webserver', function () {
  const server = gls.new('../ssr/bin/http.js')
  server.start()

  /* Assets */
  browserSync.init({
    files: '../build/**',
    proxy: 'localhost:53202',
    port: 8080,
    watchOptions: { ignoreInitial: true }
  })

  gulp.watch('../ssr/**/**').on('change', function () {
    server.start.bind(server)()
  })
})

gulp.task('app.webserver', gulp.parallel([
  'start-webserver'
]))
