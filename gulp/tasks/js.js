/* Requires */

const gulp = require('gulp')
const browserify = require('browserify')
const babel = require('gulp-babel')
const uglify = require('gulp-uglify')

const source = require('vinyl-source-stream')
const buffer = require('vinyl-buffer')
const es = require('event-stream')

const notify = require('gulp-notify')
const plumber = require('gulp-plumber')
const path = require('path')

const mode = require('gulp-mode')()

/* Settings */

const babelConfiguracoes = {
  minified: true,
  comments: false,
  presets: ['@babel/preset-env']
}

const errorMessage = 'Error: <%= error.message %>'
const route = {
  source: path.join(__dirname, '../../source/js'),
  build: path.join(__dirname, '../../build/js')
}

const files = [
  route.source + '/pages/afiliado-de-sucesso/afiliado-de-sucesso.js',
  route.source + '/pages/black/black.js',
  route.source + '/pages/consultor-de-sucesso/consultor-de-sucesso.js',
  route.source + '/pages/index/index.js'
]

/* Tasks */

gulp.task('bundle-pages', function (done) {
  const tasks = files.map(entry => {
    const entryName = entry.split('/')[entry.split('/').length - 1]

    return browserify(entry)
      .bundle()
      .pipe(source(entryName))
      .pipe(buffer())
      .pipe(plumber({ errorHandler: error => { notify.onError(errorMessage)(error) } }))
      .pipe(mode.production(babel(babelConfiguracoes)))
      .pipe(mode.production(uglify()))
      .pipe(gulp.dest(route.build + '/pages'))
  })

  es.merge.apply(null, tasks)
  es.merge(tasks).on('end', done)
})

gulp.task('app.js', gulp.parallel([
  'bundle-pages'
]))
