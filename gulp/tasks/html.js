/* Requires */

const gulp = require('gulp')
const path = require('path')

const notify = require('gulp-notify')
const plumber = require('gulp-plumber')
const mode = require('gulp-mode')()

const handlebars = require('gulp-compile-handlebars')
const htmlmin = require('gulp-htmlmin')
const rename = require('gulp-rename')

/* Settings */

const errorMessage = 'Error: <%= error.message %>'
const route = {
  source: path.join(__dirname, '../../source/html'),
  build: path.join(__dirname, '../../build/html'),
  config: path.join(__dirname, '../../ssr/config')
}

const optionsHTML = {
  collapseWhitespace: true,
  removeComments: true
}

const version = require(`${route.config}/versionamento.json`)

/* Methods */

function bundlePage (_page) {
  const PATH_PAGE = `${route.source}/pages/${_page}/${_page}.hbs`
  const PATH_PARTIALS = `${route.source}/pages/${_page}/partials`
  const PATH_STRUCTURES = `${route.source}/structures`
  const PATH_OTHER = `${route.source}/other`

  const templateData = {
    versao: {
      css: version.css,
      js: version.js
    }
  }
  const templateOptions = {
    ignorePartials: true,
    batch: [PATH_STRUCTURES, PATH_PARTIALS, PATH_OTHER]
  }

  return function () {
    return gulp.src(PATH_PAGE)
      .pipe(plumber({ errorHandler: error => { notify.onError(errorMessage)(error) } }))
      .pipe(handlebars(templateData, templateOptions))
      .pipe(rename({ extname: '.html' }))
      .pipe(mode.production(htmlmin(optionsHTML)))
      .pipe(gulp.dest(route.build + '/pages'))
      .on('end', () => { })
  }
}

/* Tasks */

gulp.task('app.html', gulp.series([
  bundlePage('afiliado-de-sucesso'),
  bundlePage('black'),
  bundlePage('consultor-de-sucesso'),
  bundlePage('erro'),
  bundlePage('index')
]))
