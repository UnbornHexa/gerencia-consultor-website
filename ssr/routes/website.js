/* Requires */

const express = require('express')
const router = express.Router()

/* Controllers */

const AFILIADO_CONTROLLER = require('../controllers/afiliado')
const BLACK_CONTROLLER = require('../controllers/black')
const CONSULTOR_CONTROLLER = require('../controllers/consultor')
const INDEX_CONTROLLER = require('../controllers/index')

/* Methods */

router.get('/', INDEX_CONTROLLER.renderizar)
router.get('/afiliado-de-sucesso', AFILIADO_CONTROLLER.renderizar)
router.get('/black', BLACK_CONTROLLER.renderizar)
router.get('/consultor-de-sucesso', CONSULTOR_CONTROLLER.renderizar)

module.exports = router
