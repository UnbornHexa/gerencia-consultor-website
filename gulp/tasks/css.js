/* Requires */

const gulp = require('gulp')
const uglifyCss = require('gulp-uglifycss')
const cssImport = require('gulp-cssimport')
const autoprefixer = require('gulp-autoprefixer')

const es = require('event-stream')

const notify = require('gulp-notify')
const plumber = require('gulp-plumber')
const path = require('path')

const mode = require('gulp-mode')()

/* Settings */

const errorMessage = 'Error: <%= error.message %>'
const route = {
  source: path.join(__dirname, '../../source/css'),
  build: path.join(__dirname, '../../build/css')
}

const files = [
  route.source + '/pages/afiliado-de-sucesso/afiliado-de-sucesso.css',
  route.source + '/pages/black/black.css',
  route.source + '/pages/consultor-de-sucesso/consultor-de-sucesso.css',
  route.source + '/pages/erro/erro.css',
  route.source + '/pages/index/index.css'
]

/* Tasks */

gulp.task('bundle-pages', function (done) {
  const tasks = files.map(entry => {
    return gulp.src(entry)
      .pipe(plumber({ errorHandler: error => { notify.onError(errorMessage)(error) } }))
      .pipe(cssImport())
      .pipe(autoprefixer())
      .pipe(mode.production(uglifyCss()))
      .pipe(gulp.dest(route.build))
  })
  es.merge.apply(null, tasks)
  es.merge(tasks).on('end', done)
})

gulp.task('app.css', gulp.parallel([
  'bundle-pages'
]))
