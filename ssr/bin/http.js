/* Requires */

const app = require('../app')
const http = require('http')

/* Port */

const port = '53202'

/* Server */

app.set('port', port)
const server = http.createServer(app)

server.listen(port)
server.on('error', onError)
server.on('listening', onListening)

console.log('# Servidor Iniciado!')
console.log('# Escutando na porta ' + port + '...')

/* Events */

function onError (error) {
  if (error.syscall !== 'listen') {
    throw error
  }

  const bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port

  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges')
      process.exit(1)
    case 'EADDRINUSE':
      console.error(bind + ' is already in use')
      process.exit(1)
    default:
      throw error
  }
}

function onListening () {
  const addr = server.address()
  const bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port
  console.log('Listening on ' + bind)
}

/* App Error Handler */

process.on('unhandledRejection', (error) => {
  console.log(error)
})
