/* Requires */

const gulp = require('gulp')

const notify = require('gulp-notify')
const plumber = require('gulp-plumber')
const path = require('path')

/* Settings */

const errorMessage = 'Error: <%= error.message %>'
const route = {
  source: path.join(__dirname, '../../source/docs'),
  build: path.join(__dirname, '../../build/docs')
}

/* Tasks */

gulp.task('bundle-docs', function () {
  return gulp.src(route.source + '/**')
    .pipe(plumber({ errorHandler: error => { notify.onError(errorMessage)(error) } }))
    .pipe(gulp.dest(route.build))
})

gulp.task('app.docs', gulp.parallel([
  'bundle-docs'
]))
