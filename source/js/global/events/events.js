/* Requires */

const WINDOW = require('./window')

/* Module */

const Module = () => {
  const methods = {}

  methods.habilitarTodosOsEventosGlobais = () => {
    WINDOW().habilitarBloqueioDragAndDrop()
  }

  return methods
}

module.exports = Module
