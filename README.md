1. Baixando o Projeto
```
git clone https://gitlab.com/gerencia-consultor/frontend/website.git
```

2. Instalando as Dependências
```
cd website
yarn install
```

3. Executando a Aplicação
```
yarn run dev
```
